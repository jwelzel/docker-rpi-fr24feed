FROM resin/rpi-raspbian:stretch

MAINTAINER Jochen Welzel

RUN apt-get update && apt-get upgrade && apt-get install wget dirmngr libcap2-bin iputils-ping

WORKDIR /tmp

RUN wget https://repo-feed.flightradar24.com/rpi_binaries/fr24feed_1.0.19-15_armhf.tgz && \
    tar xvf fr24feed_1.0.19-15_armhf.tgz && \
    mv /tmp/fr24feed_armhf/fr24feed /usr/local/bin/fr24feed && \
    chmod 755 /usr/local/bin/fr24feed

RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY startup.sh /bin/startup.sh

RUN chmod 755 /bin/startup.sh

CMD startup.sh
