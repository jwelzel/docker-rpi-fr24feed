# Docker Container with fr24feed  

### ENV Variables
```
FR_KEY=XXXXXXXXXXX
FR_DUMPHOST=dump1090-piaware
```

### Usage
To get a FR24-feederkey run it in the first step with
```
docker run --rm -it jwelzel/rpi-fr24feed /usr/local/bin/fr24feed --signup
```
If this is done and you got your key start it normally.

### Thanks
* To Frederik Granna for his [blog post](http://www.sysrun.io/2015/11/20/a-complete-docker-rpi-rtl-sdr-adsbacars-solution/) and some docker/code inspirations
* To Glenn Stewart for his [piaware containers](https://hub.docker.com/u/inodes/) 
