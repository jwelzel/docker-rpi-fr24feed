#!/bin/bash

cat << EOF >> /etc/fr24feed.ini
receiver="beast-tcp"
fr24key="$FR_KEY"
host="$FR_DUMPHOST:30005"
bs="no"
raw="no"
logmode="0"
logpath="/var/log/fr24feed"
mlat="yes"
mlat-without-gps="yes"
EOF

fr24feed --key=$FR_KEY --host=$FR_DUMPHOST:30005 --receiver=beast-tcp
